#Python alpine is lighter that Python
FROM python:3.7-alpine

#RUN apt-get update \
#    && apt-get install -y --no-install-recommends postgresql-client \
#    && rm -rf /var/lib/apt/lists/*

#With Alpine -> use apk to update and install postgresql

RUN apk update && apk add libpq

RUN addgroup -S app \
    && adduser -S app -G app

RUN mkdir /home/app/web

WORKDIR /home/app/web

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . .

RUN chown -R app:app .

USER app

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# docker build -t <image_name> -f <Dockerfile>